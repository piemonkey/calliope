Examples
========

Commandline-based examples
--------------------------

These examples use Calliope's :doc:`commandline API <reference-cli>` to build
`shell pipelines <https://effective-shell.com/docs/part-2-core-skills/7-thinking-in-pipelines/>`_
combining different commands.  Take them and play with them, modify them, break
them, and improve them.

.. toctree::
   :maxdepth: 2

   examples/simple
   examples/collectors
   examples/listen-history
   examples/similar-tracks
   examples/misc

Python examples
---------------

These more in-depth examples use the :mod:`Python API <calliope>`. They are
installed as a `calliope_examples` Python module, so you can execute them
directly, for example:

The examples are:

.. toctree::
   :maxdepth: 1

   autoapi/examples/special_mix/special_mix/index

Full reference for examples module:

.. toctree::
   :maxdepth: 1

   autoapi/examples/index

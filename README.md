```
  oooooooo8             o888  o888  o88
o888     88   ooooooo    888   888  oooo   ooooooo  ooooooooo    ooooooooo8
888           ooooo888   888   888   888 888     888 888    888 888oooooo8
888o     oo 888    888   888   888   888 888     888 888    888 888
 888oooo88   88ooo88 8o o888o o888o o888o  88ooo88   888ooo88     88oooo888
                                                    o888
```

Calliope is a toolkit for working with playlists of music.

The goal is to enable research and experimentation in open source music
recommendation, by providing a set of standard interfaces and operations
to online and local music services.

In order words, a way to mash up data from Spotify, Musicbrainz, Last.fm
and your local music collection!

For more information, see the manual:
<https://calliope-music.readthedocs.io/en/latest/>
